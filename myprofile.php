<?php

  // re-create session
  session_start();

  require_once "connector/connect.php"; 
  
  require "objects/staffControl.php";

  //Declare Page
  $page = "Profile";

    if (!isset($_SESSION['staffid'])) {
        header("location: index");
    } else {
      $staffid = $_SESSION['staffid'];
      $getStaff = mysqli_fetch_object(mysqli_query($conn, "SELECT * FROM staff WHERE staffid = '$staffid'"));
      $email = $getStaff->email;
      $title = $getStaff->title;
      $lastname = $getStaff->lastname;
      $firstname = $getStaff->firstname;
      $othername = $getStaff->othername;

      if (is_null($othername)) {
        $fullname = $lastname.', '.$firstname;
      } else {
        $fullname = $lastname.', '.$firstname.' '.$othername;
      }
      $profession = $getStaff->profession;
      $departmentId = $getStaff->departmentId;

      $researchgate = $getStaff->researchgate;
      $linkedin = $getStaff->linkedin;
      $facebook = $getStaff->facebook;
      $twitter = $getStaff->twitter;
      $bio = $getStaff->bio;
      $twitter = $getStaff->twitter;
      $staffStatus = $getStaff->status;

      $getDepartment = mysqli_fetch_object(mysqli_query($conn, "SELECT * FROM departments WHERE id = '$departmentId'"));   
      $department = $getDepartment->department;   
      $facultyId = $getDepartment->facultyId;   

      $getFaculty = mysqli_fetch_object(mysqli_query($conn, "SELECT * FROM faculty WHERE id = '$facultyId'"));

      $faculty = $getFaculty->faculty;

      if ($staffStatus == 0) {
        header("location: editbio");
      } else {

      }
    }

?>

<!DOCTYPE html>
<html lang="en">

  <!-- Head -->
  <?php require "objects/head.php";?>

<body class="profile-page sidebar-collapse">
    <div class="se-pre-con"></div>

  <!-- Navbar -->
  <?php require "objects/nav.php"; ?>

  <!-- End Navbar -->
  <div class="wrapper">
    <div class="page-header clear-filter" filter-color="blue">
      <div class="page-header-image" data-parallax="true" style="background-image:url('../assets/img/bg5.jpg');">
      </div>
      <div class="container">


        <?php
              $getPhoto = mysqli_fetch_object(mysqli_query($conn,  "SELECT * FROM staff WHERE staffid = '$staffid'"));  
              $photo = $getPhoto->photo;                     

          if ($photo != null) {
            echo '<div class="photo-container">
            
              <img src="photos/'.$photo.'" >
              </div>            
              ';
          } else {
            echo '<div class="photo-container">
            
              <img src="photos/avatar.png" >
              </div>            
              ';
          }

        ?> 


        <h3 class="title"><?php echo $fullname.' ('.$title.')'; ?></h3>
        <!--<p><?php echo $staffid; ?></p>-->
        <p class="category"><?php echo $profession; ?></p>
        <p class="category">Faculty: <?php echo $faculty; ?></p>
        <p class="category">Department: <?php echo $department; ?></p>

        <a href="#"  rel="tooltip" title="Edit Bio" data-toggle="modal" data-target="#editBio" class="btn btn-warning btn-sm btn-info"><i class="fas fa-edit"></i> Edit Bio</a>

      </div>
    </div>
    <div class="section">
      <div class="container">
        <div class="button-container">
            <!-- Social -->
            <?php require "objects/social.php"; ?>
        </div>
        <h3 class="title">About me</h3>
        <h5 class="description">
          <?php echo $bio; ?>
          <br/>
          <a href="" class="btn btn-info btn-sm" rel="tooltip" title="Edit About Me">
            <i class="fas fa-edit"></i> Edit
          </a>
        </h5>
          

        <div class="row">
            <!-- Portfolio -->
            <?php //require "objects/portfolio.php"; ?>          
        </div>

      </div>
    </div>



      <!-- Footer -->
      <?php require "objects/footer.php"; ?>
  </div>
  <!--   Core JS Files   -->
  <script src="assets/js/core/jquery.min.js" type="text/javascript"></script>
  <script src="assets/js/core/popper.min.js" type="text/javascript"></script>
  <script src="assets/js/core/bootstrap.min.js" type="text/javascript"></script>
  <!--  Plugin for Switches, full documentation here: http://www.jque.re/plugins/version3/bootstrap.switch/ -->
  <script src="assets/js/plugins/bootstrap-switch.js"></script>
  <!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
  <script src="assets/js/plugins/nouislider.min.js" type="text/javascript"></script>
  <!--  Plugin for the DatePicker, full documentation here: https://github.com/uxsolutions/bootstrap-datepicker -->
  <script src="assets/js/plugins/bootstrap-datepicker.js" type="text/javascript"></script>
  <!--  Google Maps Plugin    -->
  <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
  <!-- Control Center for Now Ui Kit: parallax effects, scripts for the example pages etc -->
  <script src="assets/js/now-ui-kit.js?v=1.2.0" type="text/javascript"></script>

  
</body>

</html>