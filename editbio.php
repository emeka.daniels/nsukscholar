<?php

  clearstatcache();
  
  // re-create session
  session_start();

  require_once "connector/connect.php"; 
  
  require "objects/staffControl.php";

  //Declare Page
  $page = "Profile";

    if (!isset($_SESSION['staffid'])) {
        header("location: index");
    } else {

    }

  /*
  if(isset($_GET['editbio'])) { 
    $_SESSION['editbio'] = $_GET['editbio'];
    header("location: functions/myprofilefunction.php?editbio");
  }
  */

?>

<!DOCTYPE html>
<html lang="en">

  <!-- Head -->
  <?php require "objects/head.php";?>

<body class="profile-page sidebar-collapse">
    <div class="se-pre-con"></div>

  <!-- Navbar -->
  <?php require "objects/nav.php"; ?>

  <!-- End Navbar -->
  <div class="wrapper">
    <div class="page-header clear-filter" filter-color="blue">
      <div class="page-header-image" data-parallax="true" style="background-image:url('../assets/img/bg5.jpg');">
      </div>
      <div class="container">

        <h3 class="title"><?php echo $staff->email; ?></h3>
        <p class="category"><?php echo $staffid; ?></p>       
          

        <?php
              $getPhoto = mysqli_fetch_object(mysqli_query($conn,  "SELECT * FROM staff WHERE staffid = '$staffid'"));  
              $photo = $getPhoto->photo;                     

          if ($photo != null) {
            echo '<div class="photo-container">
            
              <img src="photos/'.$photo.'" >
              </div>
              <div class="container upload-photo-form">
                <a href="functions/myprofilefunction.php?delete_photo='.$staffid.'" class="btn btn-xs btn-danger" data-toggle="tooltip" data-placement="top" title="Delete Photo"><i class="fas fa-times"></i> Delete Photo</a>
              </div> 
              <div>
                <hr/>
              </div>             
              ';
          } 

          else { ?>

              <div class="photo-container">
                
                <img src="photos/avatar.png" alt="">
              </div>

              <div class="container upload-photo-form">
                <form action="functions/myprofilefunction.php" method="POST" enctype="multipart/form-data" class="form-inline">           
                  <div>
                    <input type="file" class="file-input form-control" name="file" id="my_file" required/>
                    <button class="btn btn-primary btn-round" type="submit" name= "addphoto" id="img_upload"><i class="fas fa-save"></i> Upload</button>
                  </div> 
                </form> 
              </div>

              <div>
                <hr/>
              </div>
                                         
        <?php
          }
        ?>          

        <form action="functions/myprofilefunction.php" method="POST">

              <div class="row">
                <div class="col-sm-3">
                  <div class="form-group">
                    <select class="form-control" placeholder="Title" name="title" required>
                      <option value="" disabled hidden selected>Title</option>                       
                      <?php
                        $title = mysqli_query($conn, "SELECT * FROM title order by title");
                        while($row = mysqli_fetch_object($title))
                        { 
                        echo '<option value="'.$row->id.'">'.$row->title.'</option>';
                        }
                      ?>                       
                    </select>
                  </div>
                </div>                
                <div class="col-sm-3">
                  <div class="form-group">
                    <input type="text" value="<?php echo $staff->lastname; ?>" name="lastname" placeholder="Lastname" class="form-control" required/>
                  </div>
                </div>
                <div class="col-sm-3">
                  <div class="form-group">
                    <input type="text" value="<?php echo $staff->firstname; ?>" name="firstname" placeholder="Firstname" class="form-control" required/>
                  </div>
                </div>

                <div class="col-sm-3">
                  <div class="form-group">
                    <input type="text" value="<?php echo $staff->othername; ?>" name="othername" placeholder="Othername" class="form-control"/>
                  </div>
                </div>                
              </div>

              <div class="row">

                <div class="col-sm-4">
                  <div class="form-group">
                    <select class="form-control" placeholder="Gender" name="gender" required>
                      <option value="" disabled hidden selected>Gender</option>                       
                      <option value="Male">Male</option>                        
                      <option value="Female">Female</option>                        
                    </select>
                  </div>
                </div> 

                <div class="col-sm-4">
                  <div class="form-group">
                    <input type="text" value="<?php echo $staff->profession; ?>" name="profession" placeholder="Profession" class="form-control" required/>
                  </div>
                </div>

                <div class="col-sm-4">
                  <div class="form-group">
                    <select class="form-control" placeholder="Department" name="department" required>
                      <option value="" disabled hidden selected>Department</option>                       
                      <?php
                        $department = mysqli_query($conn, "SELECT * FROM departments order by department");
                        while($row = mysqli_fetch_object($department))
                        { 
                        echo '<option value="'.$row->id.'">'.$row->department.'</option>';
                        }
                      ?>                       
                    </select>
                  </div>
                </div>

              </div>

              <div>
                <hr/>
              </div>

              <div class="row">
                <div class="col-sm-3">
                  <div class="input-group input-lg" data-toggle="tooltip" data-placement="top" title="Eg. https://www.researchgate.net/profile/johndoe" data-container="body" data-animation="true" data-delay="100">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><img src="images/researchgate.png" width=15px/></span>
                    </div>
                    <input type="text" class="form-control" placeholder="Research Gate Url" name="researchgate">
                  </div>
                </div>

                <div class="col-sm-3">
                  <div class="input-group input-lg" data-toggle="tooltip" data-placement="top" title="Eg. https://www.linkedin.com/in/johndoe" data-container="body" data-animation="true" data-delay="100">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class="fab fa-linkedin"></i></span>
                    </div>
                    <input type="text" class="form-control" placeholder="LinkedIn Url" name="linkedin">
                  </div>
                </div>

                <div class="col-sm-3">
                  <div class="input-group input-lg" data-toggle="tooltip" data-placement="top" title="Eg. https://www.facebook.com/johndoe" data-container="body" data-animation="true" data-delay="100">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class="fab fa-facebook-square"></i></span>
                    </div>
                    <input type="text" class="form-control" placeholder="Facebook Url" name="facebook">
                  </div>
                </div>

                <div class="col-sm-3">
                  <div class="input-group input-lg" data-toggle="tooltip" data-placement="top" title="Eg. https://twitter.com/johndoe" data-container="body" data-animation="true" data-delay="100">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class="fab fa-twitter-square"></i></span>
                    </div>
                    <input type="text" class="form-control" placeholder="Twitter Url" name="twitter">
                  </div>
                </div>                
              </div>

              <div>
                <hr/>
              </div>
              
              <!-- ABOUT ME-->
              <div class="row">
                <div class="col-sm-12">                
                  <div class="textarea-container form-group">
                    <textarea class="form-control" name="bio" rows="4" cols="80" placeholder="Tell us about you..."></textarea>
                  </div>
                </div>
              </div>

              <div>
                <hr/>
              </div>
              
            <div>
              <button type="submit" name= "updatebio" class="btn btn-info btn-round btn-lg"><i class="fas fa-save"></i> Update </button>
            </div>    
        </form> 
      </div>
    </div>


      <!-- Footer -->
      <?php require "objects/footer.php"; ?>
  </div>
  <!--   Core JS Files   -->
  <script src="assets/js/core/jquery.min.js" type="text/javascript"></script>
  <script src="assets/js/core/popper.min.js" type="text/javascript"></script>
  <script src="assets/js/core/bootstrap.min.js" type="text/javascript"></script>
  <!--  Plugin for Switches, full documentation here: http://www.jque.re/plugins/version3/bootstrap.switch/ -->
  <script src="assets/js/plugins/bootstrap-switch.js"></script>
  <!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
  <script src="assets/js/plugins/nouislider.min.js" type="text/javascript"></script>
  <!--  Plugin for the DatePicker, full documentation here: https://github.com/uxsolutions/bootstrap-datepicker -->
  <script src="assets/js/plugins/bootstrap-datepicker.js" type="text/javascript"></script>
  <!--  Google Maps Plugin    -->
  <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
  <!-- Control Center for Now Ui Kit: parallax effects, scripts for the example pages etc -->
  <script src="assets/js/now-ui-kit.js?v=1.2.0" type="text/javascript"></script>

<script type="text/javascript">

  $('#img_upload').click(function(){
      var file = $('input[type=file]#my_file').val();
      var exts = ['png','jpg','jpeg'];//extensions
      //the file has any value?
      if ( file ) {
        // split file name at dot
        var get_ext = file.split('.');
        // reverse name to check extension
        get_ext = get_ext.reverse();
        // check file type is valid as given in 'exts' array
        if ( $.inArray ( get_ext[0].toLowerCase(), exts ) > -1 ){
          
        } else {
          alert( 'Invalid file type!' );
      return false;
        }
      }
    });

    $('#my_file').on('change',function(){
        //get the file name
        var fileName = $(this).val();
        //replace the "Choose a file" label
        //$(this).next('.file-label').html(fileName);
    });

     
</script>
  
</body>

</html>