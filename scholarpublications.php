<?php

  // re-create session
  session_start();

  require_once "connector/connect.php"; 
 
  //require "objects/staffControl.php";

  //Declare Page
  $page = "My Publications";

    if (!isset($_GET['scholar'])) {
        header("location: index");
    } else {
      $scholarid = $_GET['scholar'];
      $getStaff = mysqli_fetch_object(mysqli_query($conn, "SELECT * FROM staff WHERE staffid = '$scholarid'"));

          $title = $getStaff->title;
          $lastname = $getStaff->lastname;
          $firstname = $getStaff->firstname;
          $othername = $getStaff->othername;
          $scholarphoto = $getStaff->photo;
          $profession = $getStaff->profession;

          if (is_null($othername)) {
            $fullname = $lastname.', '.$firstname;
          } else {
            $fullname = $lastname.', '.$firstname.' '.$othername;
          }

      $departmentId = $getStaff->departmentId;

      $getDepartment = mysqli_fetch_object(mysqli_query($conn, "SELECT * FROM departments WHERE id = '$departmentId'"));   
      $department = $getDepartment->department;   
      $facultyId = $getDepartment->facultyId;   

      $getFaculty = mysqli_fetch_object(mysqli_query($conn, "SELECT * FROM faculty WHERE id = '$facultyId'"));

      $faculty = $getFaculty->faculty;


      $staffStatus = $getStaff->status;

      if ($staffStatus == 0) {
        header("location: editbio");
      } else {

      }
    }

?>

<!DOCTYPE html>
<html lang="en">

  <!-- Head -->
  <?php require "objects/head.php"; ?>

<body class="index-page sidebar-collapse">
    <div class="se-pre-con"></div>
  
  <!-- Navbar -->
  <?php require "objects/nav.php"; ?>

  <!-- End Navbar -->
  <div class="wrapper">

    <div class="main">

      <div class="section section-basic" id="basic-elements">
        <div class="container">
          <h2 class="title"><?php echo $fullname.' ('.$title.')';?></h2>

          <div class="row">
            <div class="col-md-8">
              <!-- ResearchListing -->
              <?php require "objects/scholarResearch.php"; ?> 
              
            </div>

            <div class="col-md-4">
              <!-- SideBar -->
              <?php require "objects/myPublicationSidebar.php"; ?> 
            </div>
            
          </div>

        </div>
      </div>

    <!-- Sart Modal -->
    <div class="modal fade" id="addPublication" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header justify-content-center">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
              <i class="now-ui-icons ui-1_simple-remove"></i>
            </button>
            <h4 class="title title-up">Modal title</h4>
          </div>
          <div class="modal-body">
            <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean. A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country, in which roasted parts of sentences fly into your mouth.
            </p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default">Nice Button</button>
            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
          </div>
        </div>
      </div>
    </div>
    <!--  End Modal -->

      <!-- Footer -->
      <?php require "objects/footer.php"; ?>
    </div>


  </div>
  <!--   Core JS Files  -->
  <script src="https://code.jquery.com/jquery-3.3.1.js" type="text/javascript"></script>
  <script src="assets/js/core/popper.min.js" type="text/javascript"></script>
  <script src="assets/js/core/bootstrap.min.js" type="text/javascript"></script>
  <!--  Plugin for Switches, full documentation here: http://www.jque.re/plugins/version3/bootstrap.switch/ -->
  <script src="assets/js/plugins/bootstrap-switch.js"></script>
  <!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
  <script src="assets/js/plugins/nouislider.min.js" type="text/javascript"></script>
  <!--  Plugin for the DatePicker, full documentation here: https://github.com/uxsolutions/bootstrap-datepicker -->
  <script src="assets/js/plugins/bootstrap-datepicker.js" type="text/javascript"></script>
  <!--  Google Maps Plugin    -->
  <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
  <!-- Control Center for Now Ui Kit: parallax effects, scripts for the example pages etc -->
  <script src="assets/js/now-ui-kit.js?v=1.2.0" type="text/javascript"></script>


<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>



  <script>
    $(document).ready(function() {
      // the body of this function is in assets/js/now-ui-kit.js
      nowuiKit.initSliders();
    });

    function scrollToDownload() {

      if ($('.section-download').length != 0) {
        $("html, body").animate({
          scrollTop: $('.section-download').offset().top
        }, 1000);
      }
    }

  $(document).ready(function(){
    $.fn.dataTable.ext.classes.sPageButton = 'button button-primary'; // Change Pagination Button Class
    $('#researchTable').dataTable({
      
        "paging":   true,
        "ordering": false,
        "info":     true,
        "pagingType": "full"
    });
  });   




  </script>
</body>

</html>