  <!-- Edit Bio Modal -->
  <div class="modal modal-warning fade" id="editBio" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header justify-content-center">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title">Edit Scholar Bio</h4>
        </div>
        <form action="functions/myprofilefunction.php" method="POST">
            <div class="modal-body">
              
              <p class="modal-subtitle"><?php echo $staff->email.' ('.$staffid.')'; ?></p>

              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <input type="text" value="<?php echo $staff->lastname; ?>" name="lastname" placeholder="Lastname" class="form-control" required/>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <input type="text" value="<?php echo $staff->firstname; ?>" name="firstname" placeholder="Firstname" class="form-control" required/>
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <input type="text" value="<?php echo $staff->othername; ?>" name="othername" placeholder="Othername" class="form-control"/>
                  </div>
                </div>

                <div class="col-sm-6">
                  <div class="form-group">
                    <select class="form-control" placeholder="Gender" name="gender" required>
                      <option value="" disabled hidden selected>Gender</option>                       
                      <option value="Male">Male</option>                        
                      <option value="Female">Female</option>                        
                    </select>
                  </div>
                </div>                
              </div>

              <div>
                <hr/>
              </div>
              
              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <input type="text" value="<?php echo $staff->profession; ?>" name="profession" placeholder="Profession" class="form-control" required/>
                  </div>
                </div>

                <div class="col-sm-6">
                  <div class="form-group">
                    <select class="form-control" placeholder="Department" name="department" required>
                      <option value="" disabled hidden selected>Department</option>                       
                      <?php
                        $department = mysqli_query($conn, "SELECT * FROM departments order by department");
                        while($row = mysqli_fetch_object($department))
                        { 
                        echo '<option value="'.$row->id.'">'.$row->department.'</option>';
                        }
                      ?>                       
                    </select>
                  </div>
                </div>
              </div>

              <div>
                <hr/>
              </div>
              
              <!-- ABOUT ME -->
              <div class="row">
                <div class="textarea-container form-group">
                  <textarea class="form-control" name="bio" rows="4" cols="80" placeholder="Tell us about you..."></textarea>
                </div>
              </div>

              <div>
                <hr/>
              </div>
              

              <div class="row">
                <div class="col-sm-6">
                  <div class="input-group input-lg" data-toggle="tooltip" data-placement="top" title="Eg. https://www.linkedin.com/in/johndoe" data-container="body" data-animation="true" data-delay="100">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class="fab fa-linkedin"></i></span>
                    </div>
                    <input type="text" class="form-control" placeholder="LinkedIn Url" name="linkedin">
                  </div>
                </div>

                <div class="col-sm-6">
                  <div class="input-group input-lg" data-toggle="tooltip" data-placement="top" title="Eg. https://www.facebook.com/johndoe" data-container="body" data-animation="true" data-delay="100">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class="fab fa-facebook-square"></i></span>
                    </div>
                    <input type="text" class="form-control" placeholder="Facebook Url" name="facebook">
                  </div>
                </div>
               
              </div>

              <div class="row">
                <div class="col-sm-6">
                  <div class="input-group input-lg" data-toggle="tooltip" data-placement="top" title="Eg. https://twitter.com/johndoe" data-container="body" data-animation="true" data-delay="100">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class="fab fa-twitter-square"></i></span>
                    </div>
                    <input type="text" class="form-control" placeholder="Twitter Url" name="twitter">
                  </div>
                </div> 
              </div>

            </div>

            <div class="modal-footer">
              <button type="button" class="btn btn-warning" data-dismiss="modal">Exit</button>
              <button type="submit" name= "updatebio" class="btn btn-info"><i class="fas fa-save"></i> Update </button>
            </div>    
        </form> 
      </div>
    </div>
  </div>