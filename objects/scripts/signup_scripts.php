<script>

	window.setTimeout(function() {
		$("#alert").fadeTo(500, 0).slideUp(500, function(){
			$(this).remove(); 
			var stateObj = {};
			window.history.pushState(stateObj, "", "signup");			
		});
	}, 4000);	

	window.setTimeout(function() {
		$("#existAlert").fadeTo(500, 0).slideUp(500, function(){
			$(this).remove(); 
			var stateObj = {};
			window.history.pushState(stateObj, "", "signup");			
			window.location="login";			
		});
	}, 4000);	

	window.setTimeout(function() {
		$("#successAlert").fadeTo(500, 0).slideUp(500, function(){
			$(this).remove(); 
			var stateObj = {};
			window.history.pushState(stateObj, "", "signup");			
			window.location="login";			
		});
	}, 4000);	
</script>
