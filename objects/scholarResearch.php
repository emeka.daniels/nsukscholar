          <table id="researchTable" class="table table-hover">
            <thead><tr><th></th> </tr> </thead>
            <tbody>

<?php
              $all = mysqli_query($conn,  "SELECT * FROM publications where staffid = '$scholarid' order by datepublished DESC");
                  while($publications = mysqli_fetch_object($all))
                  {

                    $scholarid = $publications->staffid;
                    $getStaff = mysqli_fetch_object(mysqli_query($conn, "SELECT * FROM staff WHERE staffid = '$scholarid'"));
                        $lastname = $getStaff->lastname;
                        $firstname = $getStaff->firstname;
                        $othername = $getStaff->othername;

                        if (is_null($othername)) {
                          $fullname = $lastname.', '.$firstname;
                        } else {
                          $fullname = $lastname.', '.$firstname.' '.$othername;
                        }

                  $fileid = $publications->id;
                  $title = $publications->title;
                  $datepublished = date_create($publications->datepublished);
                  $abstract = $publications->abstract;
                  $pdf = $publications->file;


        ?>

              <tr>
                <td class="mainlisting">
                  <div class="row">
                    <div class="title col-md-12">
                      <a href="publication?view=<?php echo $fileid; ?>"><?php echo $title; ?></a>
                    </div>
                    <div class="date col-md-12">Date Uploaded: <?php echo date_format($datepublished,"h:iA d F, Y");?></div>
                    <div class="extract col-md-12">
                      <?php 
                        if (strlen($abstract) > 400) {
                          echo substr($abstract,0,400)."... "; ?>
                          <span class="readmore"><a class="btn btn-warning btn-sm" href="publication?view=<?php echo $fileid; ?>">Read More</a></span>
                          <?php                         
                        } else {
                          echo $abstract;
                        }

                      ?>
                    </div>                    
                    
                    <div class = "keywords col-sm-12">
                      <!-- <b><i class="fas fa-tags"></i> Keywords</b> -->
                      <b>Keywords : </b><a class="btn btn-info btn-sm" href="#"><i class="fas fa-tag"></i> Published</a>
                      <a class="btn btn-info btn-sm" href="#"><i class="fas fa-tag"></i> NSUK</a>
                      <a class="btn btn-info btn-sm" href="#"><i class="fas fa-tag"></i> Scholar</a>
                      <a class="btn btn-info btn-sm" href="#"><i class="fas fa-tag"></i> Research</a>
                    </div>
                  </div>                                       
                  <span class="sexy_line"></span>
                </td>
              </tr> 
<?php 
    }
?>          
            </tbody>
          </table>