          <table id="scholarTable" class="table" cellspacing="10">
            <thead><tr><th></th> </tr> </thead> <!-- Important for Search and filter -->
            <tbody>
              <?php

                $all = mysqli_query($conn, "SELECT * FROM staff WHERE status = 1 order by lastname ASC");
                while($getStaff = mysqli_fetch_object($all)) {

                    $scholarid = $getStaff->staffid;
                    $email = $getStaff->email;
                    $title = $getStaff->title;
                    $lastname = $getStaff->lastname;
                    $firstname = $getStaff->firstname;
                    $othername = $getStaff->othername;

                    if (is_null($othername)) {
                      $fullname = $lastname.', '.$firstname;
                    } else {
                      $fullname = $lastname.', '.$firstname.' '.$othername;
                    }
                    
                    $photo = $getStaff->photo;

                    $sex = $getStaff->sex;
                    $profession = $getStaff->profession;
                    $departmentId = $getStaff->departmentId;

                    $linkedin = $getStaff->linkedin;
                    $facebook = $getStaff->facebook;
                    $twitter = $getStaff->twitter;
                    $bio = $getStaff->bio;
                    $researchgate = $getStaff->researchgate;
                    $staffStatus = $getStaff->status;

                        if ($sex == "Male") {
                          $sexcolor = "#3f3dc6";
                        } else {
                          $sexcolor = "#CF4191";
                        }
                        
                        $gender = "
                            <button class='btn btn-round btn-sm' style='background:".$sexcolor."'>
                              <i class='fas fa-".strtolower($sex)."'></i>
                            </button>
                          ";

                    $getDepartment = mysqli_fetch_object(mysqli_query($conn, "SELECT * FROM departments WHERE id = '$departmentId'"));   
                    $department = $getDepartment->department;   
                    $facultyId = $getDepartment->facultyId;   

                    $getFaculty = mysqli_fetch_object(mysqli_query($conn, "SELECT * FROM faculty WHERE id = '$facultyId'"));
                    $faculty = $getFaculty->faculty;

                    $journalCount = mysqli_num_rows(mysqli_query($conn, "SELECT * FROM publications WHERE staffid = '$scholarid'"));                    
              ?>

              <tr>
                <td class="mainListing">
                  <div class="row">
                    <div class="col-md-3">
                      <?php
                      if ($photo != null) {
                        echo '<div class="photo-container">
                        
                          <img src="photos/'.$photo.'" >
                          </div>            
                          ';
                      } else {
                        echo '<div class="photo-container">';
                            if ($sex == "Male") {
                              echo '<img src="photos/avatar.png" >';
                            } else {
                              echo '<img src="photos/female.png" >';
                            }                                                  
                        echo '</div>';
                      }
                      require "objects/listingsocial.php";
                      ?>                      
                    </div>

                    <div class="col-md-9">
                      <table style="font-size: 0.9em;" class="table childListing">
                        <tr><td colspan=2><?php echo "<span class='name'><span class='fullname'>".$gender." <a href='profile?scholar=".$scholarid."' rel='tooltip' title='".$fullname."'>".$fullname."</span> (".$title.")</a></span><br/><b>Department: </b>".$department;?></td>
                          <td>
                            <?php echo
                            '<a href="mailto:'.$email.'" target="_blank" class="btn btn-round btn-sm btn-icon" style="background: #18CE0F;" rel="tooltip" title="Email">
                              <i class="fab fa-edge"></i>
                            </a>';
                            ?>                            
                          </td></tr>

                        <tr><td colspan=3>
                          <?php
                            if (strlen($bio) > 145) {
                              echo substr($bio,0,145)."... "; ?>
                              <?php                         
                            } else {
                              echo $bio;
                            }
                          ?> 
                          <hr/>
                          <?php 
                            if ($journalCount > 0) {
                              echo '<a href="scholarpublications?scholar='.$scholarid.'" class="btn btn-primary btn-sm btn-round pull-right">
                                <i class="fas fa-newspaper fa-lg"></i> Publications ('.$journalCount.')
                              </a>';
                            } else {
                              echo '<a href="#" class="btn btn-primary btn-sm btn-round pull-right">
                                <i class="fas fa-newspaper fa-lg"></i> Publications ('.$journalCount.')
                              </a>';
                            }
                          ?>                                               
                          </td>
                        </tr>                       
                      </table>
                    </div>
                  </div>
                </td>
              </tr> 
<?php 

    }
?>          
            </tbody>
          </table>