<?php
    echo '
      <a href="mailto:'.$email.'" target="_blank" class="btn btn-default btn-round btn-lg btn-icon" rel="tooltip" title="Email">
        <i class="fab fa-edge"></i>
      </a>
    ';
  if (is_null($researchgate)) {

  } else {
        echo '
          <a href="'.$researchgate.'" target="_blank" class="btn btn-round btn-lg btn-icon" style="background: #00CCBB;" rel="tooltip" title="Research Gate">
            <img src="images/rgater.png"/>
          </a>
        ';
  }

  if (is_null($linkedin)) {

  } else {
        echo '
          <a href="'.$linkedin.'" target="_blank" class="btn btn-round btn-lg btn-icon" style="background: #0274B3;" rel="tooltip" title="LinkedIn">
            <i class="fab fa-linkedin"></i>
          </a>
        ';
  }

  if (is_null($facebook)) {

  } else {
        echo '
          <a href="'.$facebook.'" target="_blank" class="btn btn-round btn-lg btn-icon" style="background: #4267B2;" rel="tooltip" title="Facebook">
            <i class="fab fa-facebook"></i>
          </a>
        ';    
  }

  if (is_null($twitter)) {

  } else {
        echo '
          <a href="'.$twitter.'" target="_blank" class="btn btn-round btn-lg btn-icon"style="background: #1DA1F2;" rel="tooltip" title="Twitter">
            <i class="fab fa-twitter"></i>
          </a>
        ';      
  } 
?>