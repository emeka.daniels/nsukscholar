

<?php
  if (isset($staffid)) { //If Staff is Logged In
  ?>
  <nav class="navbar navbar-expand-lg bg-success fixed-top navbar-transparent" >
    <div class="container">
      <div class="navbar-translate">
        <i class="fas fa-book fa-lg"></i> 
        <a class="navbar-brand" style="font-size: 1.5em;" href="index.php" rel="tooltip" title="" data-placement="bottom">
           NSUK Scholar
        </a>
        <button class="navbar-toggler navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-bar top-bar"></span>
          <span class="navbar-toggler-bar middle-bar"></span>
          <span class="navbar-toggler-bar bottom-bar"></span>
        </button>
      </div>

      <div class="collapse navbar-collapse justify-content-end" id="navigation" data-nav-image="assets/img/blurred-image-1.jpg">
        <ul class="navbar-nav">
          <li class="nav-item">
            <a class="nav-link" href="index" rel="tooltip" title="View all Scholars" data-placement="bottom">
              <i class="fas fa-home fa-lg"></i>
              Home
            </a>
          </li>

          <li class="nav-item">
            <a class="nav-link" href="publications">
              <i class="fas fa-newspaper fa-lg"></i>
              Journals/Publications
            </a>
          </li>

          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle"  id="navbarDropdownMenuLink1" data-toggle="dropdown" href="myprofile">
              <i class="fas fa-user"> </i>
              <?php echo $staffid; ?>
            </a>
            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink1">
                  <a class="dropdown-item" href="mypublications">
                    <i class="fas fa-newspaper fa-lg"></i>
                  My Publications
                  </a>
              <?php if ($staffStatus == 1) {
                ?>
                  <a class="dropdown-item" href="addpublication">
                    <i class="fas fa-plus fa-lg"></i>
                  Add Publications
                  </a>
                <?php
              } else {
                ?>
                  <a class="dropdown-item disabled" href="#" rel="tooltip" title="Update Profile Information to Add Publication" data-placement="bottom">
                    <i class="fas fa-plus fa-lg"></i>
                    Add Publication
                  </a>                
                <?php
              }

              ?>
            </div>            
          </li>

          <li class="nav-item">
            <a class="nav-link btn btn-neutral" href="logout">
              <i class="fas fa-sign-out-alt"></i>
              Log Out
            </a>
          </li>

          <?php
            if ($staffStatus == 1) {
              //echo "status is 1";        
            } else {
              //echo "status is 0";              
            }

          ?>
        </ul>
      </div>
    </div>
  </nav>
  <?php
  }




  //GUEST NAVIGATION 
  else { //If Guest
  ?>
  <nav class="navbar navbar-expand-lg bg-success fixed-top navbar-transparent" >
    <div class="container">
      <div class="navbar-translate">
        <i class="fas fa-book fa-lg"></i>  
        <a class="navbar-brand" style="font-size: 1.5em;" href="index.php" rel="tooltip" title="" data-placement="bottom">
           NSUK Scholar
        </a>
        <button class="navbar-toggler navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-bar top-bar"></span>
          <span class="navbar-toggler-bar middle-bar"></span>
          <span class="navbar-toggler-bar bottom-bar"></span>
        </button>
      </div>
      <div class="collapse navbar-collapse justify-content-end" id="navigation" data-nav-image="assets/img/blurred-image-1.jpg">
        <ul class="navbar-nav">
          <li class="nav-item">
            <a class="nav-link" href="index" rel="tooltip" title="View all Scholars" data-placement="bottom">
              <i class="fas fa-home fa-lg"></i>
              Home
            </a>
          </li>

          <li class="nav-item">
            <a class="nav-link" href="publications">
              <i class="fas fa-newspaper fa-lg"></i>
              Journals/Publications
            </a>
          </li>

          <li class="nav-item">
            <a class="nav-link btn btn-neutral" href="login">
              <i class="fas fa-sign-in-alt"></i>
              Log In
            </a>
          </li>

        </ul>
      </div>
    </div>
  </nav>
  <?php
  }
?>