<?php 
      if (isset($_GET['error'])) {
        echo '
        <div class="alert alert-danger" role="alert" id="alert">
            <div class="alert-icon">
              <i class="now-ui-icons ui-2_like"></i>
            </div>
            <strong>Error!</strong> Wrong Staff ID and/or Email.
        </div>'; 
      }

      if (isset($_GET['exists'])) {
        echo '
        <div class="alert alert-info" role="alert" id="existAlert">
            You have already Signed Up. <a class="" href="login.php">Login</a> to continue.           
        </div>'; 
      }

      if (isset($_GET['success'])) {
        echo '
        <div class="alert alert-success" role="alert" id="successAlert">
            <strong>Congratulations!</strong> Please check your email for your password.
        </div>'; 
      }

      if (isset($_GET['failed'])) {
        echo '
        <div class="alert alert-warning" role="alert" id="alert">
            <strong>Oops!</strong> Please try again.
        </div>'; 
      }

      if (isset($_GET['signup'])) {
        echo '
        <div class="alert alert-warning" role="alert" id="">
            You have not signed up, yet.<br/>Click <a href="signup" style="" class="btn btn-sm"><b>Sign Up Now</b></a> to continue.
        </div>'; 
      }      
?>