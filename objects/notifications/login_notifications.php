<?php 
	      if (isset($_GET['error'])) {
	        echo '
	        <div class="alert alert-danger" role="alert" id="alert">
	            <div class="alert-icon">
	              <i class="now-ui-icons ui-2_like"></i>
	            </div>
	            <strong>Error!</strong> Wrong Staff ID and/or Email.
	        </div>'; 
	      }

	      if (isset($_GET['success'])) {
	        echo '
	        <div class="alert alert-success" role="alert" id="loginSuccess">
				<i class="fas fa-unlock-alt"></i> Login Successful. <i class="fas fa-spinner"></i> Redirecting...
	        </div>'; 
	      }
?>