
<?php 
  if (is_null($scholarphoto)) {
    $scholarphoto = "avatar.png";
  }
?>

    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
      <!--<div class="viewAuthor">Author</div>-->
      <center>
        <div>        
          <img src="photos/<?php echo $scholarphoto; ?>" alt="<?php echo $fullname; ?>" class="rounded img-raised scholarsidebar"> 
        </div>
        <a class="btn btn-primary btn-sm" href="profile?scholar=<?php echo $scholarid; ?>"><i class="now-ui-icons users_circle-08"></i> <?php echo $fullname; ?></a>
        <div id="sidebarDetails">
          <span class="profession">- <?php echo $profession; ?> -</span>        
            <div style="clear: both;"> </div>
          <span class="faculty">Faculty: <?php echo $faculty; ?></span>        
            <div style="clear: both;"> </div>
          <span class="department">Department: <?php echo $department; ?></span>
        </div>              
      </center>


    </div>