<?php
  
  // re-create session
  session_start();

  clearstatcache();

  require_once "connector/connect.php"; 

  require "objects/staffControl.php";

  //Declare Page
  $page = "Error";

?>

<!DOCTYPE html>
<html lang="en">

  <!-- Head -->
  <?php require "objects/head.php"; ?>

<body class="index-page sidebar-collapse">
    <div class="se-pre-con"></div>

  <!-- End Navbar -->
  <div class="wrapper">

    <div class="main">

      <div class="section section-basic" id="basic-elements">
        <div class="container">

        <center>
          <i class="fa fa-exclamation-triangle" style="font-size: 150px; color:red;" aria-hidden="true"></i>

          <h3>Oops! Something went wrong.</h3>
          <p>Failed to connect to the database.</p>
          <p>Please <a href="index"><span class="label label-info">Try Again</span></a> or contact the administrator.</p>
        </center>

        </div>
      </div>

    </div>


  </div>
  <!--   Core JS Files  -->
  <script src="https://code.jquery.com/jquery-3.3.1.js" type="text/javascript"></script>
  <script src="assets/js/core/popper.min.js" type="text/javascript"></script>
  <script src="assets/js/core/bootstrap.min.js" type="text/javascript"></script>
  <!--  Plugin for Switches, full documentation here: http://www.jque.re/plugins/version3/bootstrap.switch/ -->
  <script src="assets/js/plugins/bootstrap-switch.js"></script>
  <!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
  <script src="assets/js/plugins/nouislider.min.js" type="text/javascript"></script>
  <!--  Plugin for the DatePicker, full documentation here: https://github.com/uxsolutions/bootstrap-datepicker -->
  <script src="assets/js/plugins/bootstrap-datepicker.js" type="text/javascript"></script>
  <!--  Google Maps Plugin    -->
  <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
  <!-- Control Center for Now Ui Kit: parallax effects, scripts for the example pages etc -->
  <script src="assets/js/now-ui-kit.js?v=1.2.0" type="text/javascript"></script>


<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>



  <script>
    $(document).ready(function() {
      // the body of this function is in assets/js/now-ui-kit.js
      nowuiKit.initSliders();
    });

    function scrollToDownload() {

      if ($('.section-download').length != 0) {
        $("html, body").animate({
          scrollTop: $('.section-download').offset().top
        }, 1000);
      }
    }

  $(document).ready(function(){
    $.fn.dataTable.ext.classes.sPageButton = 'button button-primary'; // Change Pagination Button Class
    $('#researchTable').dataTable({
      
        "paging":   true,
        "ordering": false,
        "info":     true,
        "pagingType": "full"
    });
  });   




  </script>
</body>

</html>