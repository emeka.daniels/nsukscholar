<?php
//ADD STAFF SCRIPT
if(isset($_POST['signup'])) {

		function test_input($data) {
				$data = trim($data);
				$data = stripslashes($data);
				$data = htmlspecialchars($data, ENT_QUOTES, 'UTF-8');
				return $data;
		}
		if ($_SERVER["REQUEST_METHOD"] == "POST") {
				$staffid = test_input($_POST["staffid"]);				
				$email = test_input($_POST["email"]);				
		}
		$check = mysqli_query($conn, "SELECT * FROM staff WHERE staffid = '$staffid' and email = '$email'");				

		if (mysqli_num_rows($check) < 1) {
			/*$_SESSION['roleid'] = $staffrole;
			$_SESSION['loginid'] = $loginid;*/
			header("location: signup?error");
		}
		else {
			//User Exists
			$getPassword = mysqli_fetch_object(mysqli_query($conn, "SELECT * FROM staff WHERE staffid = '$staffid' and email = '$email'"));
			$password = $getPassword->password;

			    if (is_null($password)) {

					$length = 6;
					$randomPassword = "";
					$characters = array_merge(range('A','Z'), range('a','z'), range('0','9'));
					$max = count($characters) - 1;
					for ($i = 0; $i < $length; $i++) {
						$rand = mt_rand(0, $max);
						$randomPassword .= $characters[$rand];
						$password = md5($randomPassword);
					}

	                $subject = 'Login Authentication';
                	$message = '<p>Hello '.$staffid.',<br/>You have initiated your registration on the NSUK Scholar Portal.</p>
               			<p>Your password is '.$randomPassword.'<br/>Please follow the link below to login to the portal and change your password.<br/><a href="'.$url.'login">NSUK SCHOLAR LOGIN</a></p>';

                    $to = $email; //email
                    
                    $headers = "MIME-Version: 1.0\r\n";
                    $headers .= "Content-type: text/html\r\n";
                    $headers .= 'From: "'.$appName.'" <'.$mailer.'>';
                    
                    $sentmail = mail($to, $subject, $message, $headers);
                

		            if ($sentmail) {
						$edit = "UPDATE staff set password = '$password' WHERE staffid = '$staffid' and email = '$email'";
						$edited = mysqli_query($conn, $edit) or die(mysqli_error($conn));

						if ($edited) {
							header("location: signup?success");
						}  
						else {
                       		header("location: signup?failed");   			
						} 
		            }
		   				    
			        else {
                        header("location: signup?failed"); 			
			        }


				}
				else {
					header("location: signup?exists");
				}
		}

}

//FORGOT PASSWORD SCRIPT
if(isset($_POST['forgotpassword'])) {

		function test_input($data) {
				$data = trim($data);
				$data = stripslashes($data);
				$data = htmlspecialchars($data, ENT_QUOTES, 'UTF-8');
				return $data;
		}
		if ($_SERVER["REQUEST_METHOD"] == "POST") {
				$staffid = test_input($_POST["staffid"]);				
				$email = test_input($_POST["email"]);				
		}
		$check = mysqli_query($conn, "SELECT * FROM staff WHERE staffid = '$staffid' and email = '$email'");				

		if (mysqli_num_rows($check) < 1) {
			/*$_SESSION['roleid'] = $staffrole;
			$_SESSION['loginid'] = $loginid;*/
			header("location: forgotpassword?error");
		}
		else {			

			//User Exists
			$getPassword = mysqli_fetch_object(mysqli_query($conn, "SELECT * FROM staff WHERE staffid = '$staffid' and email = '$email'"));
			$password = $getPassword->password;

			    if (is_null($password)) {
					header("location: forgotpassword?signup");
				}
				else {

					$length = 6;
					$randomPassword = "";
					$characters = array_merge(range('A','Z'), range('a','z'), range('0','9'));
					$max = count($characters) - 1;
					for ($i = 0; $i < $length; $i++) {
						$rand = mt_rand(0, $max);
						$randomPassword .= $characters[$rand];
						$password = md5($randomPassword);
					}

	                $subject = 'Login Authentication';
                	$message = '<p>Hello '.$staffid.',<br/>You have requested a password reset on the NSUK Scholar Portal.</p>
               			<p>Your new password is '.$randomPassword.'<br/>Please follow the link below to login and continue.<br/><a href="'.$url.'login">NSUK SCHOLAR LOGIN</a></p>';

                    $to = $email; //email
                    
                    $headers = "MIME-Version: 1.0\r\n";
                    $headers .= "Content-type: text/html\r\n";
                    $headers .= 'From: "'.$appName.'" <'.$mailer.'>';
                    
                    $sentmail = mail($to, $subject, $message, $headers);
                

		            if ($sentmail) {
						$edit = "UPDATE staff set password = '$password' WHERE staffid = '$staffid' and email = '$email'";
						$edited = mysqli_query($conn, $edit) or die(mysqli_error($conn));

						if ($edited) {
							header("location: forgotpassword?success");
						}  
						else {
                       		header("location: forgotpassword?failed");   			
						} 
		            }
		   				    
			        else {
                        header("location: forgotpassword?failed"); 			
			        }					
				}
		}

}
?>