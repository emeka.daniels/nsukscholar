<?php


  require_once "../connector/connect.php"; 

  // re-create session
  session_start();

//EDIT PROFILE SCRIPT
if(isset($_POST['updatebio'])) {
$staffid = $_SESSION["staffid"];


function test_input($data) {
		$data = trim($data);
		$data = stripslashes($data);
		$data = htmlspecialchars($data, ENT_QUOTES, 'UTF-8');
		return $data;
}

if ($_SERVER["REQUEST_METHOD"] == "POST") {
		$lastname = test_input($_POST["lastname"]);
		$firstname = test_input($_POST["firstname"]);
		$othername = test_input($_POST["othername"]);
		$sex = test_input($_POST["gender"]);
		$profession = test_input($_POST["profession"]);
		$department = test_input($_POST["department"]);
		$researchgate = test_input($_POST["researchgate"]);
		$linkedin = test_input($_POST["linkedin"]);
		$facebook = test_input($_POST["facebook"]);
		$twitter = test_input($_POST["twitter"]);
		$bio = test_input($_POST["bio"]);
}

		$date = date("Y-m-d h:i:s");
		$status = 1;

				$check = mysqli_query($conn, "SELECT * FROM staff WHERE staffid = '$staffid'");

				if (mysqli_num_rows($check) > 0) {
				
						$edit = "UPDATE staff set biodate = '$date', lastname = '$lastname', firstname = '$firstname', othername = '$othername', sex = '$sex', profession = '$profession', departmentId = '$department', researchgate = '$researchgate', linkedin = '$linkedin', facebook = '$facebook', twitter = '$twitter', bio = '$bio', status = '$status' WHERE staffid = '$staffid'";
						$edited = mysqli_query($conn, $edit) or die(mysqli_error($conn));
												
						if ($edited) {								
						 		header("location: ../myprofile?edited");																	
						}
						else {
						 		header("location: ../myprofile?failed");			
						}
				}
			
				else {
						header("location: ../myprofile?failed");		
				}/**/
}


//ADD PHOTO ONLY
if(isset($_POST['addphoto'])) {

 $staffid = $_SESSION['staffid'];

	function getExtension($str) {
         $i = strrpos($str,".");
         if (!$i) { return ""; }
         $l = strlen($str) - $i;
         $ext = substr($str,$i+1,$l);
         return $ext;
	} 

	$image=$_FILES['file']['name'];

	if ($image) {

		//echo "E fetch";
		$filename = stripslashes($_FILES['file']['name']);
	  	$extension = getExtension($filename);				

			$photo = date("ymdHis").$_FILES['file']['name'];
				$newname="../photos"."/".$photo;
				$copied = copy($_FILES['file']['tmp_name'], $newname);

				/*echo "newname is ".$newname;
				echo "<br/>Photo is ".$photo;
				echo "<br/>Staff ID is ".$staffid;*/
				
				$check = mysqli_query($conn, "SELECT * FROM staff WHERE staffid = '$staffid'");

				if ($check) {
				
						$update = "UPDATE staff set photo = '$photo' WHERE staffid = '$staffid'";
						$updated = mysqli_query($conn, $update) or die(mysqli_error($conn));
												
						if ($update) {								
							$projectUpdated = "projectUpdated";
							$_SESSION['projectUpdated'] = $projectUpdated;
							header("location: ../editbio?photouploaded");																	
						}
						else {
							$Failed = "Failed";
							$_SESSION['Failed'] = $Failed;
							header("location: ../editbio?photofailed");	
						}


				}
			
				else {
							$Failed = "Failed";
							$_SESSION['Failed'] = $Failed;
							header("location: ../editbio?photofailed");	
				}/**/

	} 
	else {

	}
}


if(isset($_GET['delete_photo'])) {
	
	$staffid  = $_GET["delete_photo"];
	
	$check = mysqli_fetch_object(mysqli_query($conn, "SELECT * FROM staff WHERE staffid = '$staffid'"));

	$file = $check->photo;


	$filepath = "../photos/".$file;

	unlink($filepath);
	

		$edit = "UPDATE staff set photo = NULL WHERE staffid = '$staffid'";
		$edited = mysqli_query($conn, $edit) or die(mysqli_error($conn));	
		
		header("location: ../editbio?photo_deleted"); 	


				

}

?>