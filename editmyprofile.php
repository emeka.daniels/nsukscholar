<?php
 
  // re-create session
  session_start();

  require_once "connector/connect.php"; 
 
  require "objects/staffControl.php";

  //Declare Page
  $page = "Profile";

    if (!isset($_SESSION['staffid'])) {
        header("location: index");
    } else {
      //header("location: profile");
    }

  /*
  if(isset($_GET['editbio'])) { 
    $_SESSION['editbio'] = $_GET['editbio'];
    header("location: functions/myprofilefunction.php?editbio");
  }
  */

?>

<!DOCTYPE html>
<html lang="en">

  <!-- Head -->
  <?php require "objects/head.php";?>

<body class="profile-page sidebar-collapse">

  <!-- Navbar -->
  <?php require "objects/nav.php"; ?>

  <!-- End Navbar -->
  <div class="wrapper">
    <div class="page-header clear-filter" filter-color="blue">
      <div class="page-header-image" data-parallax="true" style="background-image:url('../assets/img/bg5.jpg');">
      </div>
      <div class="container">
        <div class="photo-container">
          <img src="../assets/img/ryan.jpg" alt="">
        </div>
        <h3 class="title">Doe, John K.</h3>
        <!--<p><?php echo $staffid; ?></p>-->
        <p class="category">Lecturer</p>
        <p class="category">Faculty of Agriculture - Department of Plant Sciences</p>

        <a href="myprofile?editbio=<?php echo $staffid; ?>"  rel="tooltip" title="Edit Bio" data-toggle="modal" data-target="#editBio" class="btn btn-warning btn-sm btn-info"><i class="fas fa-edit"></i> Edit Bio</a>
        <!--
        <div class="content">
          <div class="social-description">
            <h2>26</h2>
            <p>Comments</p>
          </div>
          <div class="social-description">
            <h2>26</h2>
            <p>Comments</p>
          </div>
          <div class="social-description">
            <h2>48</h2>
            <p>Bookmarks</p>
          </div>
        </div>
        -->
      </div>
    </div>
    <div class="section">
      <div class="container">
        <div class="button-container">
          <!--<a href="#button" class="btn btn-primary btn-round btn-lg">Follow</a>-->
          <a href="" class="btn btn-default btn-round btn-lg btn-icon" rel="tooltip" title="Follow me on Twitter">
            <i class="fab fa-linkedin"></i>
          </a>
          <a href="" class="btn btn-default btn-round btn-lg btn-icon" rel="tooltip" title="Follow me on Twitter">
            <i class="fab fa-twitter"></i>
          </a>
          <a href="" class="btn btn-default btn-round btn-lg btn-icon" rel="tooltip" title="Follow me on Instagram">
            <i class="fab fa-instagram"></i>
          </a>
        </div>
        <h3 class="title">About me</h3>
        <h5 class="description">An artist of considerable range, Ryan — the name taken by Melbourne-raised, Brooklyn-based Nick Murphy — writes, performs and records all of his own music, giving it a warm, intimate feel with a solid groove structure. An artist of considerable range.
          <br/>
          <a href="" class="btn btn-info btn-sm" rel="tooltip" title="Edit About Me">
            <i class="fas fa-edit"></i> Edit
          </a>
        </h5>
          

        <div class="row">
            <div class="col-md-12 ml-auto col-xl-12 mr-auto">
              <h4 class="title text-center">My Portfolio</h4>
              <!-- Tabs with Background on Card -->
              <div class="card">
                <div class="card-header">
                  <ul class="nav nav-tabs justify-content-center" role="tablist">
                    <li class="nav-item">
                      <a class="nav-link active" data-toggle="tab" href="#home1" role="tab"><i class="now-ui-icons shopping_cart-simple"></i> Home</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" data-toggle="tab" href="#profile1" role="tab">Profile</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" data-toggle="tab" href="#messages1" role="tab">Messages</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" data-toggle="tab" href="#settings1" role="tab">Settings</a>
                    </li>
                  </ul>
                </div>
                <div class="card-body">
                  <!-- Tab panes -->
                  <div class="tab-content text-center">
                    <div class="tab-pane active" id="home1" role="tabpanel">
                      <p>I think that’s a responsibility that I have, to push possibilities, to show people, this is the level that things could be at. So when you get something that has the name Kanye West on it, it’s supposed to be pushing the furthest possibilities. I will be the leader of a company that ends up being worth billions of dollars, because I got the answers. I understand culture. I am the nucleus.</p>
                    </div>
                    <div class="tab-pane" id="profile1" role="tabpanel">
                      <p> I will be the leader of a company that ends up being worth billions of dollars, because I got the answers. I understand culture. I am the nucleus. I think that’s a responsibility that I have, to push possibilities, to show people, this is the level that things could be at. I think that’s a responsibility that I have, to push possibilities, to show people, this is the level that things could be at. </p>
                    </div>
                    <div class="tab-pane" id="messages1" role="tabpanel">
                      <p>I think that’s a responsibility that I have, to push possibilities, to show people, this is the level that things could be at. So when you get something that has the name Kanye West on it, it’s supposed to be pushing the furthest possibilities. I will be the leader of a company that ends up being worth billions of dollars, because I got the answers. I understand culture. I am the nucleus.</p>
                    </div>
                    <div class="tab-pane" id="settings1" role="tabpanel">
                      <p>
                        "I will be the leader of a company that ends up being worth billions of dollars, because I got the answers. I understand culture. I am the nucleus. I think that’s a responsibility that I have, to push possibilities, to show people, this is the level that things could be at."
                      </p>
                    </div>
                  </div>
                </div>
              </div>
              <!-- End Tabs on plain Card -->
            </div>
        </div>
      </div>
    </div>

  <!-- Edit Bio Modal -->
  <div class="modal modal-warning fade" id="editBio" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header justify-content-center">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title">Edit Scholar Bio</h4>
        </div>
        <form action="functions/myprofilefunction.php" method="POST">
            <div class="modal-body">
              
              <p class="modal-subtitle"><?php echo $staff->email.' ('.$staffid.')'; ?></p>

              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <input type="text" value="<?php echo $staff->lastname; ?>" name="lastname" placeholder="Lastname" class="form-control" required/>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <input type="text" value="<?php echo $staff->firstname; ?>" name="firstname" placeholder="Firstname" class="form-control" required/>
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <input type="text" value="<?php echo $staff->othername; ?>" name="othername" placeholder="Othername" class="form-control"/>
                  </div>
                </div>

                <div class="col-sm-6">
                  <div class="form-group">
                    <select class="form-control" placeholder="Gender" name="gender" required>
                      <option value="" disabled hidden selected>Gender</option>                       
                      <option value="Male">Male</option>                        
                      <option value="Female">Female</option>                        
                    </select>
                  </div>
                </div>                
              </div>

              <div>
                <hr/>
              </div>
              
              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <input type="text" value="<?php echo $staff->profession; ?>" name="profession" placeholder="Profession" class="form-control" required/>
                  </div>
                </div>

                <div class="col-sm-6">
                  <div class="form-group">
                    <select class="form-control" placeholder="Department" name="department" required>
                      <option value="" disabled hidden selected>Department</option>                       
                      <?php
                        $department = mysqli_query($conn, "SELECT * FROM departments order by department");
                        while($row = mysqli_fetch_object($department))
                        { 
                        echo '<option value="'.$row->id.'">'.$row->department.'</option>';
                        }
                      ?>                       
                    </select>
                  </div>
                </div>
              </div>

              <div>
                <hr/>
              </div>
              
              <!-- ABOUT ME
              <div class="row">
                <div class="textarea-container form-group">
                  <textarea class="form-control" name="bio" rows="4" cols="80" placeholder="Tell us about you..."></textarea>
                </div>
              </div>

              <div>
                <hr/>
              </div>
              -->

              <div class="row">
                <div class="col-sm-6">
                  <div class="input-group input-lg" data-toggle="tooltip" data-placement="top" title="Eg. https://www.linkedin.com/in/johndoe" data-container="body" data-animation="true" data-delay="100">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class="fab fa-linkedin"></i></span>
                    </div>
                    <input type="text" class="form-control" placeholder="LinkedIn Url" name="linkedin">
                  </div>
                </div>

                <div class="col-sm-6">
                  <div class="input-group input-lg" data-toggle="tooltip" data-placement="top" title="Eg. https://www.facebook.com/johndoe" data-container="body" data-animation="true" data-delay="100">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class="fab fa-facebook-square"></i></span>
                    </div>
                    <input type="text" class="form-control" placeholder="Facebook Url" name="facebook">
                  </div>
                </div>
               
              </div>

              <div class="row">
                <div class="col-sm-6">
                  <div class="input-group input-lg" data-toggle="tooltip" data-placement="top" title="Eg. https://twitter.com/johndoe" data-container="body" data-animation="true" data-delay="100">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class="fab fa-twitter-square"></i></span>
                    </div>
                    <input type="text" class="form-control" placeholder="Twitter Url" name="twitter">
                  </div>
                </div> 
              </div>

            </div>

            <div class="modal-footer">
              <button type="button" class="btn btn-warning" data-dismiss="modal">Exit</button>
              <button type="submit" name= "updatebio" class="btn btn-info"><i class="fas fa-save"></i> Update </button>
            </div>    
        </form> 
      </div>
    </div>
  </div>

      <!-- Footer -->
      <?php require "objects/footer.php"; ?>
  </div>
  <!--   Core JS Files   -->
  <script src="assets/js/core/jquery.min.js" type="text/javascript"></script>
  <script src="assets/js/core/popper.min.js" type="text/javascript"></script>
  <script src="assets/js/core/bootstrap.min.js" type="text/javascript"></script>
  <!--  Plugin for Switches, full documentation here: http://www.jque.re/plugins/version3/bootstrap.switch/ -->
  <script src="assets/js/plugins/bootstrap-switch.js"></script>
  <!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
  <script src="assets/js/plugins/nouislider.min.js" type="text/javascript"></script>
  <!--  Plugin for the DatePicker, full documentation here: https://github.com/uxsolutions/bootstrap-datepicker -->
  <script src="assets/js/plugins/bootstrap-datepicker.js" type="text/javascript"></script>
  <!--  Google Maps Plugin    -->
  <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
  <!-- Control Center for Now Ui Kit: parallax effects, scripts for the example pages etc -->
  <script src="assets/js/now-ui-kit.js?v=1.2.0" type="text/javascript"></script>

  
</body>

</html>