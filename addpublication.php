<?php

  // re-create session
  session_start();

  clearstatcache();

  require_once "connector/connect.php"; 
  
  require "objects/staffControl.php";

  //Declare Page
  $page = "New Publication";

    if (!isset($_SESSION['staffid'])) {
        header("location: index");
    } else {
      $staffid = $_SESSION['staffid'];
      $getStaff = mysqli_fetch_object(mysqli_query($conn, "SELECT * FROM staff WHERE staffid = '$staffid'"));

      $staffStatus = $getStaff->status;

      if ($staffStatus == 0) {
        header("location: editbio");
      } else {

      }
    }

  /*
  if(isset($_GET['editbio'])) { 
    $_SESSION['editbio'] = $_GET['editbio'];
    header("location: functions/myprofilefunction.php?editbio");
  }
  */

?>

<!DOCTYPE html>
<html lang="en">

  <!-- Head -->
  <?php require "objects/head.php";?>

<body class="profile-page sidebar-collapse">
    <div class="se-pre-con"></div>

  <!-- Navbar -->
  <?php require "objects/nav.php"; ?>

  <!-- End Navbar -->
  <div class="wrapper">
    <div class="page-header clear-filter" filter-color="blue">
      <div class="page-header-image" data-parallax="true" style="background-image:url('../assets/img/bg5.jpg');">
      </div>
      <div class="container">
      <div class ="newPublication">
        <h3 class="title"><?php echo $staff->email; ?></h3>
        <p class="category"><?php echo $staffid; ?></p>       
          
        <h3 class="page-title">New Publication</h3>      

        <form action="functions/addpublicationfunction.php" method="POST" enctype="multipart/form-data">

              <div class="row">
                <div class="col-sm-9">
                  <div class="row">
                    <div class="col-sm-12">
                      <div class="form-group">
                        <input type="text" value="" name="title" placeholder="Publication Title" class="form-control" required/>
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-sm-8">
                      <div class="textarea-container form-group">
                        <textarea class="form-control" name="abstract" rows="4" cols="80" placeholder="Abstract" required></textarea>
                      </div>
                    </div>

                    <div class="col-sm-4">
                      <div class="upload-file">
                        <input type="file" class="file-input form-control" name="publication" id="uploadFile" required/>
                      </div>
                    </div>                    
                  </div>                                    
                </div>

                <div class="col-sm-3">
                  <div class="publication_info_box">
                    <b style="font-size: 1.3em;">IMPORTANT</b>
                    <p>File to be uploaded <br/><b><u>MUST</u></b> be pdf <br/><i class="far fa-file-pdf fa-lg"></i></p>
                  </div>
                </div>


              </div>

              <div>
                <button type="submit" name= "addpublication" class="btn btn-info btn-round btn-lg" id="file_upload"><i class="fas fa-save"></i> Submit Publication</button>
              </div>              
   
        </form> 

      </div>
     
      </div>
    </div>


      <!-- Footer -->
      <?php require "objects/footer.php"; ?>
  </div>
  <!--   Core JS Files   -->
  <script src="assets/js/core/jquery.min.js" type="text/javascript"></script>
  <script src="assets/js/core/popper.min.js" type="text/javascript"></script>
  <script src="assets/js/core/bootstrap.min.js" type="text/javascript"></script>
  <!--  Plugin for Switches, full documentation here: http://www.jque.re/plugins/version3/bootstrap.switch/ -->
  <script src="assets/js/plugins/bootstrap-switch.js"></script>
  <!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
  <script src="assets/js/plugins/nouislider.min.js" type="text/javascript"></script>
  <!--  Plugin for the DatePicker, full documentation here: https://github.com/uxsolutions/bootstrap-datepicker -->
  <script src="assets/js/plugins/bootstrap-datepicker.js" type="text/javascript"></script>
  <!--  Google Maps Plugin    -->
  <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
  <!-- Control Center for Now Ui Kit: parallax effects, scripts for the example pages etc -->
  <script src="assets/js/now-ui-kit.js?v=1.2.0" type="text/javascript"></script>

<script type="text/javascript">

  $('#file_upload').click(function(){
      var file = $('input[type=file]#uploadFile').val();
      var exts = ['pdf'];//extensions
      //the file has any value?
      if ( file ) {
        // split file name at dot
        var get_ext = file.split('.');
        // reverse name to check extension
        get_ext = get_ext.reverse();
        // check file type is valid as given in 'exts' array
        if ( $.inArray ( get_ext[0].toLowerCase(), exts ) > -1 ){
          
        } else {
          alert( 'Invalid file type!. Only PDF files accepted.' );
      return false;
        }
      }
    });

    $('#uploadFile').on('change',function(){
        //get the file name
        var fileName = $(this).val();
        //replace the "Choose a file" label
        //$(this).next('.file-label').html(fileName);
    });

     
</script>
  
</body>

</html>