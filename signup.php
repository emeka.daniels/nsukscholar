<?php
  // re-create session
  session_start();

  require_once ("connector/connect.php"); 

  require 'phpmailer/PHPMailerAutoload.php';  
  require_once ("functions/signupfunction.php");   

  $page = "Sign Up";


?>

<!DOCTYPE html>
<html lang="en">

  <!-- Head -->
  <?php require "objects/head.php"; ?>

  <?php require "objects/scripts/signup_scripts.php"; ?>

<body class="login-page sidebar-collapse">
  <div class="se-pre-con"></div>

  <!-- Navbar -->
  <?php require "objects/nav.php"; ?>

    <div class="page-header clear-filter" filter-color="orange">
      <div class="page-header-image" style="background-image:url(assets/img/nsuk_main_gate.gif)"></div>
      <div class="content">
        <div class="container">
          <div class="col-md-4 ml-auto mr-auto">

            <?php require "objects/notifications/signup_notifications.php"; ?>

            <div class="card card-signup" data-background-color="orange">
              <form class="form" method="post" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>">
                <div class="card-header text-center">
                  <span style="font-size: 3em;">
                    <i class="fas fa-user-plus"></i> SIGN UP
                  </span>
                </div>
                <div class="card-body">
                  <div class="input-group input-lg">
                    <div class="input-group-prepend">
                      <span class="input-group-text">
                        <i class="fas fa-user"></i>
                      </span>
                    </div>
                    <input type="text" class="form-control" placeholder="Staff ID" name = "staffid" required>
                  </div>
                  <div class="input-group input-lg">
                    <div class="input-group-prepend">
                      <span class="input-group-text">
                        <i class="now-ui-icons ui-1_email-85"></i>
                      </span>
                    </div>
                    <input type="email" class="form-control" placeholder="NSUK Email Address" name = "email" required>
                  </div>
                  <!-- If you want to add a checkbox to this form, uncomment this code -->
                  <!-- <div class="checkbox">
                <input id="checkboxSignup" type="checkbox">
                  <label for="checkboxSignup">
                  Unchecked
                  </label>
                </div> -->
                </div>

                <div class="card-footer text-center">
                  <button class="btn btn-neutral btn-round btn-lg" type="submit" name="signup">Sign Up</button>                 
                </div>
                <div class="row card-footer text-center">
                  <div class="col-sm-6 pull-left">
                    <h6>
                      <a href="Login" class="link">Login Here</a>
                    </h6>
                  </div>
                  <div class="col-sm-6 pull-right">
                    <h6>
                      <a href="forgotpassword" class="link">Forgot Password?</a>
                    </h6>
                  </div>
                </div>               
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!--
    <footer class="footer">
      <div class="container">

        <div class="copyright" id="copyright">
          &copy;
          <script>
            document.getElementById('copyright').appendChild(document.createTextNode(new Date().getFullYear()))
          </script>, Developed by
          <a href="https://www.chukwuemeka.com.ng" target="_blank">C. Daniels</a>.
        </div>
      </div>
    </footer>
    -->
  </div>
  <!--   Core JS Files   -->
  <script src="assets/js/core/jquery.min.js" type="text/javascript"></script>
  <script src="assets/js/core/popper.min.js" type="text/javascript"></script>
  <script src="assets/js/core/bootstrap.min.js" type="text/javascript"></script>
  <!--  Plugin for Switches, full documentation here: http://www.jque.re/plugins/version3/bootstrap.switch/ -->
  <script src="assets/js/plugins/bootstrap-switch.js"></script>
  <!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
  <script src="assets/js/plugins/nouislider.min.js" type="text/javascript"></script>
  <!--  Plugin for the DatePicker, full documentation here: https://github.com/uxsolutions/bootstrap-datepicker -->
  <script src="assets/js/plugins/bootstrap-datepicker.js" type="text/javascript"></script>
  <!--  Google Maps Plugin    -->
  <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
  <!-- Control Center for Now Ui Kit: parallax effects, scripts for the example pages etc -->
  <script src="assets/js/now-ui-kit.js?v=1.2.0" type="text/javascript"></script>

</body>

</html>