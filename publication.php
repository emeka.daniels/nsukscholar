<?php
  
  // re-create session
  session_start();

  require_once "connector/connect.php"; 

  require "objects/staffControl.php";

  //Declare Page
  $page = "Scholar Profile";

    if (!isset($_GET['view'])) {
        header("location: index");
    } else {
      $publicationid = $_GET['view'];
      $publications = mysqli_fetch_object(mysqli_query($conn, "SELECT * FROM publications WHERE id = '$publicationid'"));

      $scholarid = $publications->staffid;
      $getStaff = mysqli_fetch_object(mysqli_query($conn, "SELECT * FROM staff WHERE staffid = '$scholarid'"));
          $lastname = $getStaff->lastname;
          $firstname = $getStaff->firstname;
          $othername = $getStaff->othername;
          $scholarphoto = $getStaff->photo;
          $profession = $getStaff->profession;

          if (is_null($othername)) {
            $fullname = $lastname.', '.$firstname;
          } else {
            $fullname = $lastname.', '.$firstname.' '.$othername;
          }

    $fileid = $publications->id;
    $title = $publications->title;
    $datepublished = date_create($publications->datepublished);
    $abstract = $publications->abstract;
    $pdf = $publications->file;
    $downloads = $publications->count;


      $departmentId = $getStaff->departmentId;

      $getDepartment = mysqli_fetch_object(mysqli_query($conn, "SELECT * FROM departments WHERE id = '$departmentId'"));   
      $department = $getDepartment->department;   
      $facultyId = $getDepartment->facultyId;   

      $getFaculty = mysqli_fetch_object(mysqli_query($conn, "SELECT * FROM faculty WHERE id = '$facultyId'"));

      $faculty = $getFaculty->faculty;


    }

?>

<!DOCTYPE html>
<html lang="en">

  <!-- Head -->
  <?php require "objects/head.php";?>

<body class="profile-page sidebar-collapse">
    <div class="se-pre-con"></div>

  <!-- Navbar -->
  <?php require "objects/nav.php"; ?>

  <!-- End Navbar -->
  <div class="wrapper">

    <div class="main">

      <div class="section section-basic" id="basic-elements">
        <div class="container">
          <h3 class="singletitle"><?php echo $title; ?></h3>
          

          
          <div class="row">
            <div class="col-md-8">
                  <div class="row" id="researchTable">
                  <span class="sexy_line"></span>
                  <br/>
                    <div class="date col-md-12">Date Uploaded: <?php echo date_format($datepublished,"h:iA d F, Y");?></div>
                    <div class="extract col-md-12">
                      <?php 
                          echo $abstract;                          
                      ?>
                    </div>                    

                    <div class = "col-md-9 keywords">
                      <b>Keywords : </b><a class="btn btn-info btn-sm" href="#"><i class="fas fa-tag"></i> Published</a>
                      <a class="btn btn-info btn-sm" href="#"><i class="fas fa-tag"></i> NSUK</a>
                      <a class="btn btn-info btn-sm" href="#"><i class="fas fa-tag"></i> Scholar</a>
                      <a class="btn btn-info btn-sm" href="#"><i class="fas fa-tag"></i> Research</a>        
                    </div>
                    <div class="col-md-3">
                        <a class="btn btn-success btn-sm download" href="download?fileid=<?php echo $fileid; ?>"><i class="fas fa-download"></i> <span class="d-none d-md-inline-block">Download</span></a>
                    </div>
                    
                    <div class = "downloads col-sm-12">
                      <?php echo $downloads.' Downloads';?>
                    </div>
                    <span class="sexy_line"></span>
                  </div> 
              
            </div>

            <div class="col-md-4">
              <!-- SideBar -->
              <?php require "objects/publicationSidebar.php"; ?> 
            </div>
            
          </div>

        </div>
      </div>

    </div>






      <!-- Footer -->
      <?php require "objects/footer.php"; ?>
  </div>
  <!--   Core JS Files   -->
  <script src="assets/js/core/jquery.min.js" type="text/javascript"></script>
  <script src="assets/js/core/popper.min.js" type="text/javascript"></script>
  <script src="assets/js/core/bootstrap.min.js" type="text/javascript"></script>
  <!--  Plugin for Switches, full documentation here: http://www.jque.re/plugins/version3/bootstrap.switch/ -->
  <script src="assets/js/plugins/bootstrap-switch.js"></script>
  <!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
  <script src="assets/js/plugins/nouislider.min.js" type="text/javascript"></script>
  <!--  Plugin for the DatePicker, full documentation here: https://github.com/uxsolutions/bootstrap-datepicker -->
  <script src="assets/js/plugins/bootstrap-datepicker.js" type="text/javascript"></script>
  <!--  Google Maps Plugin    -->
  <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
  <!-- Control Center for Now Ui Kit: parallax effects, scripts for the example pages etc -->
  <script src="assets/js/now-ui-kit.js?v=1.2.0" type="text/javascript"></script>

  
</body>

</html>